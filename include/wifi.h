#ifndef __ESPM32_WIFI_H__
#define __ESPM32_WIFI_H__

void start_wifi_manager(void);
bool is_wifi_connected(void);
bool is_wifi_ap_open(void);

#endif