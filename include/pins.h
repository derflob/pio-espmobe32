#ifndef __ESPM32_PINS_H__
#define __ESPM32_PINS_H__

#include "driver/gpio.h"
#include "driver/adc.h"

#define LED_STATUS GPIO_NUM_27

#define PIR_INPUT GPIO_NUM_34
#define PIR_INPUT_RTC_NUM 4

#define KX023_INTERRUPT GPIO_NUM_32

#define BATTERY_SENSE_ADC1_CHANNEL ADC1_CHANNEL_7
#define BATTERY_SENSE_ADC_CHANNEL ADC_CHANNEL_7

#define SENSORS_SDA 4
#define SENSORS_SCL 0

#endif
