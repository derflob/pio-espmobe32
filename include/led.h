#ifndef __ESPM32_LED_H__
#define __ESPM32_LED_H__

void start_status_led(void);
void stop_status_led(void);

#endif