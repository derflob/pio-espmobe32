#ifndef __ESPM32_ULP_UTILS_H__
#define __ESPM32_ULP_UTILS_H__

/* This function is called once after power-on reset, to load ULP program into
 * RTC memory and configure the periphery.
 */
void init_ulp_periphery();

/* This function is called every time before going into deep sleep.
 * It starts the ULP program and enables sleep wakeup sources.
 */
void start_ulp_program();

#endif
