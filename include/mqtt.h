#ifndef __ESPM32_MQTT_H__
#define __ESPM32_MQTT_H__

#include "mqtt_client.h"

#include "cJSON.h"

esp_mqtt_client_handle_t start_mqtt(void);

bool is_mqtt_connected();

void get_unique_topic(char *name, char *type, char *buffer, size_t length);

void get_discovery_topic(char *name, char *type, char *buffer, size_t length);
void get_state_topic(char *name, char *buffer, size_t length);
void get_trigger_topic(char *name, char *buffer, size_t length);
void get_attributes_topic(char *name, char *buffer, size_t length);
void get_settings_topic(char *name, char *buffer, size_t length);

void publish_discovery_sensor(esp_mqtt_client_handle_t mqtt, char *name, char *device_class, char *state_class, char *uom, char *icon, char *attributes_name, bool force_name);
void publish_discovery_binary_sensor(esp_mqtt_client_handle_t mqtt, char *name, char *device_class, char *attributes_name, char *payload_on, char *payload_off, bool force_name);
void publish_discovery_trigger(esp_mqtt_client_handle_t mqtt, char *name, char *discovery_name, char *type, char *subtype, char *payload);

void publish_data_float_with_decimals(esp_mqtt_client_handle_t mqtt, char * name, float value, bool retain, uint8_t decimals);
void publish_data_float(esp_mqtt_client_handle_t mqtt, char * name, float value, bool retain);
void publish_data_uint32(esp_mqtt_client_handle_t mqtt, char * name, uint32_t value, bool retain);
void publish_data_int32(esp_mqtt_client_handle_t mqtt, char * name, int32_t value, bool retain);
void publish_data_string(esp_mqtt_client_handle_t mqtt, char * name, char * value, bool retain);

void publish_data_attributes(esp_mqtt_client_handle_t mqtt, char * name, cJSON *json, bool retain);

void publish_trigger(esp_mqtt_client_handle_t mqtt, char * name, char * value, bool retain);

#endif