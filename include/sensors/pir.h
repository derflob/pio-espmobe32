
#ifndef __ESPM32_SENSORS_PIR_H__
#define __ESPM32_SENSORS_PIR_H__

#include <stdbool.h>

#include "sensors/progress.h"
#include "sensors/types.h"

typedef struct sensor_pir {
    bool movement;
} sensor_pir;

void init_sensor_pir(sensor_definition *definition);

#endif
