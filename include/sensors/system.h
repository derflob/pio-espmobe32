#ifndef __ESPM32_SENSORS_SYSTEM_H__
#define __ESPM32_SENSORS_SYSTEM_H__

#include "sensors/progress.h"
#include "sensors/types.h"

typedef struct sensor_system {
    float battery_voltage;
    float battery_sense_factor;
    bool battery_low;
    float battery_voltage_low;
    unsigned long reboots;
} sensor_system;

void init_sensor_system(sensor_definition *definition);

#endif
