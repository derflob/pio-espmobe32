#ifndef __ESPM32_SENSORS_KX023_H__
#define __ESPM32_SENSORS_KX023_H__

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include "sensors/progress.h"
#include "sensors/types.h"

typedef enum kx023_registers {
    // high pass filter output
    KX023_REG_XHPL = 0x00,
    KX023_REG_XHPH,
    KX023_REG_YHPL,
    KX023_REG_YHPH,
    KX023_REG_ZHPL,
    KX023_REG_ZHPH,

    // output
    KX023_REG_XOUTL = 0x06,
    KX023_REG_XOUTH,
    KX023_REG_YOUTL,
    KX023_REG_YOUTH,
    KX023_REG_ZOUTL,
    KX023_REG_ZOUTH,

    KX023_REG_COTR,

    KX023_REG_WHO_AM_I = 0x0F,

    // current/previous tilt position
    KX023_REG_TSCP,
    KX023_REG_TSPP,
    
    // interrupt source
    KX023_REG_INS1 = 0x12, // axis triggered tap
    KX023_REG_INS2, // interrupt type
    KX023_REG_INS3, // motion direction
    KX023_REG_STAT, // any interupt triggered
    KX023_REG_INT_REL = 0x17, // latched interupt source

    // feature controls
    KX023_REG_CNTL1 = 0x18,
    KX023_REG_CNTL2,
    KX023_REG_CNTL3,
    KX023_REG_ODCNTL, // output data rate
    
    // interrupt control
    KX023_REG_INC1, // pin INT1
    KX023_REG_INC2, // motion detect enabled directions
    KX023_REG_INC3, // tep directions
    KX023_REG_INC4, // pin INT1 mapping
    KX023_REG_INC5, // pin INT2
    KX023_REG_INC6, // pin INt2 mapping

    KX023_REG_TILT_TIMER,
    KX023_REG_WUFC,
    KX023_REG_TDTRC, // tap control
    KX023_REG_TDTC, // double tap counter information
    KX023_REG_TTH, // tap threshold high
    KX023_REG_TTL, // tap theshold low
    KX023_REG_FTD, // tap counter
    KX023_REG_STD, // double tap counter information 
    KX023_REG_TLT, // tap counter information
    KX023_REG_TWS, // sinlge or double tap counter information

    KX023_REG_ATH = 0x30, // motion detect threshold

    KX023_REG_TILT_ANGLE_LL = 0x32, // tilt threshold low
    KX023_REG_TILT_ANGLE_HL, // tilt threshold high
    KX023_REG_HYST_SET, // rotation state hysteresis

    KX023_REG_LP_CNTL, // low power control (avg filter)

    KX023_REG_BUF_CNTL1 = 0x3A, // buffer sample threshold (watermark intr))
    KX023_REG_BUF_CNTL2, // buffer model
    KX023_REG_BUF_STATUS_1, // buffer length
    KX023_REG_BUF_STATUS_2, // buffer trigger

    KX023_REG_BUF_CLEAR,
    KX023_REG_BUF_READ,
    KX023_REG_SELF_TEST,
} kx023_registers;

typedef struct sensor_kx023 {
    bool single_tap;
    bool double_tap;
} sensor_kx023;

void init_sensor_kx023(sensor_definition *definition);

#endif
