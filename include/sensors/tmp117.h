#ifndef __ESPM32_SENSORS_TMP117_H__
#define __ESPM32_SENSORS_TMP117_H__

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "sensors/progress.h"
#include "sensors/types.h"

#define TMP117_TEMPERATURE_FACTOR (0.0078125)

typedef enum tmp117_registers {
    TMP117_REG_TEMP_RESULT = 0x00,
    TMP117_REG_CONFIGURATION,
    TMP117_REG_THIGH_LIMIT,
    TMP117_REG_TLOW_LIMIT,
    TMP117_REG_EEPROM_UL,
    TMP117_REG_EEPROM1,
    TMP117_REG_EEPROM2,
    TMP117_REG_TEMP_OFFSET,
    TMP117_REG_EEPROM3,
    TMP117_REG_DEVICE_ID = 0x0F
} tmp117_registers;

typedef enum tmp117_averaging_mode {
    TMP117_AVERAGING_0x,
    TMP117_AVERAGING_8x,
    TMP117_AVERAGING_32x,
    TMP117_AVERAGING_64x,
} tmp117_averaging_mode;

typedef enum tmp117_conversion_mode {
    TMP117_CONVERSION_MODE_CONTINUOUS = 0x00,
    TMP117_CONVERSION_MODE_SHUTDOWN = 0x01,
    TMP117_CONVERSION_MODE_ONESHOT = 0x03,
} tmp117_conversion_mode;

typedef struct tmp117_register_configuration {
    // MSB
    uint8_t conversion_cycle_time: 2;
    tmp117_conversion_mode conversion_mode: 2;
    bool eeprom_busy: 1;
    bool data_ready: 1;
    bool low_alert: 1;
    bool high_alert: 1;
    // LSB
    uint8_t not_used: 1;
    bool soft_reset: 1;
    uint8_t alert_pin_function: 1;
    uint8_t alert_pin_polarity: 1;
    uint8_t therm_alert_mode: 1;
    tmp117_averaging_mode avg_mode: 3;
} tmp117_register_configuration;

typedef struct sensor_tmp117 {
    float temperature;
} sensor_tmp117;

void init_sensor_tmp117(sensor_definition *);

#endif
