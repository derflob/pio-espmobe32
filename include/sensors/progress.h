#ifndef __ESPM32_SENSORS_PROGRESS_H__
#define __ESPM32_SENSORS_PROGRESS_H__

typedef enum sensor_progress {
    ESPMOBE32_SP_UNKNOWN,
    ESPMOBE32_SP_INITIALIZING,
    ESPMOBE32_SP_MEASURING,
    ESPMOBE32_SP_MEASURED,
    ESPMOBE32_SP_PUBLISHED,
    ESPMOBE32_SP_FAILED,
} sensor_progress;

#endif
