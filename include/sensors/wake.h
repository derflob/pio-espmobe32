
#ifndef __ESPM32_SENSORS_WAKE_H__
#define __ESPM32_SENSORS_WAKE_H__

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "sensors/progress.h"
#include "sensors/types.h"

typedef enum wake_reason {
    ESPMOBE32_WAKE_UNKNOWN,
    ESPMOBE32_WAKE_ULP,
    ESPMOBE32_WAKE_EXT1,
    ESPMOBE32_WAKE_PERIODIC,
} wake_reason;

typedef struct sensor_wake {
    wake_reason wake_reason;
} sensor_wake;

void init_sensor_wake(sensor_definition *);

#endif
