#ifndef __ESPM32_SENSORS_BME280_H__
#define __ESPM32_SENSORS_BME280_H__

#include <bme280.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include "sensors/progress.h"
#include "sensors/types.h"

typedef struct sensor_bme280 {
    float humidity;
    float pressure;
    float temperature;
} sensor_bme280;

void init_sensor_bme280(sensor_definition *definition);

#endif
