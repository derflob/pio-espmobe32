#ifndef __ESPM32_SENSORS_TYPES_H__
#define __ESPM32_SENSORS_TYPES_H__

#include "mqtt_client.h"

typedef enum sensor_types {
    ESPMOBE32_SENSOR_PIR,
    ESPMOBE32_SENSOR_WAKE,
    ESPMOBE32_SENSOR_KX023,
    ESPMOBE32_SENSOR_TMP117,
    ESPMOBE32_SENSOR_BME280,
    ESPMOBE32_SENSOR_OPT3001,
    ESPMOBE32_SENSOR_SYSTEM,

    ESPMOBE32_SENSOR_MAX
} sensor_types;

typedef struct sensor_handle sensor_handle;
typedef union sensor_data sensor_data;

typedef struct sensor_definition {
        enum sensor_types type;

        sensor_data *data;

        const char *name;

        void (*configure)(sensor_handle *);
        void (*task)(void *);
        void (*publish_discovery)(sensor_handle *, esp_mqtt_client_handle_t mqtt);
        void (*publish_values)(sensor_handle *, esp_mqtt_client_handle_t mqtt);
        void (*register_mqtt_topic)(sensor_handle *, esp_mqtt_client_handle_t mqtt);
        void (*abort)(sensor_handle *);
} sensor_definition;

#endif
