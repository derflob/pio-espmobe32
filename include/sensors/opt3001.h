#ifndef __ESPM32_SENSORS_OPT3001_H__
#define __ESPM32_SENSORS_OPT3001_H__

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "sensors/progress.h"
#include "sensors/types.h"

typedef enum opt3001_registers {
    OPT3001_REG_RESULT = 0x00,
    OPT3001_REG_CONFIGURATION,
    OPT3001_REG_LOW_LIMIT,
    OPT3001_REG_HIGH_LIMIT,
    OPT3001_REG_MANUFACTURER_ID = 0x7E,
    OPT3001_REG_DEVICE_ID = 0x7F,
} opt3001_registers;

typedef struct opt3001_register_result {
    uint16_t fractional: 12;
    uint16_t exponent: 4;
} opt3001_register_result;

typedef enum opt3001_fault_count {
    OPT3001_FAULT_COUNT_ONE,
    OPT3001_FAULT_COUNT_TWO,
    OPT3001_FAULT_COUNT_FOUR,
    OPT3001_FAULT_COUNT_EIGHT,
} opt3001_fault_count;

typedef enum opt3001_conversion_mode {
    OPT3001_CONVERSION_MODE_SHUTDOWN = 0x00,
    OPT3001_CONVERSION_MODE_SINGLESHOT = 0x01,
    OPT3001_CONVERSION_MODE_CONTINUOUS = 0x02,
} opt3001_conversion_mode;

typedef enum opt3001_converion_time {
    OPT3001_CONVERSION_TIME_100MS = 0x00,
    OPT3001_CONVERSION_TIME_800MS = 0x01,
} opt3001_converion_time;

typedef enum opt3001_range {
    OPT3001_RANGE_40_95,
    OPT3001_RANGE_81_90,
    OPT3001_RANGE_163_80,
    OPT3001_RANGE_327_60,
    OPT3001_RANGE_655_20,
    OPT3001_RANGE_1310_40,
    OPT3001_RANGE_2620_80,
    OPT3001_RANGE_5241_60,
    OPT3001_RANGE_10483_20,
    OPT3001_RANGE_20966_40,
    OPT3001_RANGE_41932_80,
    OPT3001_RANGE_83865_60,
    OPT3001_RANGE_AUTO,
} opt3001_range;

typedef struct opt3001_register_configuration {
    // MSB
    bool overflow: 1;
    opt3001_conversion_mode conversion_mode: 2;
    opt3001_converion_time conversion_time: 1;
    opt3001_range range: 4;

    // LSB
    opt3001_fault_count fault_count: 2;
    bool mask_exponent: 1;
    uint8_t polarity: 1;
    uint8_t latch: 1;
    bool flag_low: 1;
    bool flag_high: 1;
    bool conversion_ready: 1;
} opt3001_register_configuration;

typedef struct sensor_opt3001 {
    float illuminance;
} sensor_opt3001;

void init_sensor_opt3001(sensor_definition *definition);

#endif
