#ifndef __ESPM32_OTA_H__
#define __ESPM32_OTA_H__

#include "mqtt_client.h"

void start_ota_mqtt(esp_mqtt_client_handle_t mqtt);

bool is_ota_in_progress();
bool is_ota_performed();

#endif