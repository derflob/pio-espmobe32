#ifndef __ESPM32_I2C_H__
#define __ESPM32_I2C_H__

#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"

SemaphoreHandle_t init_i2c_bus(void);

void grab_i2c_bus(SemaphoreHandle_t mutex);
void release_i2c_bus(SemaphoreHandle_t mutex);

void read_i2c_buffer(uint8_t address, uint8_t reg, uint8_t *buffer, size_t length, SemaphoreHandle_t mutex);
void write_i2c_buffer(uint8_t address, uint8_t reg, uint8_t *buffer, size_t length, SemaphoreHandle_t mutex);

uint8_t read_i2c_byte(uint8_t address, uint8_t reg, SemaphoreHandle_t mutex);
void write_i2c_byte(uint8_t address, uint8_t reg, uint8_t byte, SemaphoreHandle_t mutex);

uint16_t read_i2c_2bytes(uint8_t address, uint8_t reg, SemaphoreHandle_t mutex);
void write_i2c_2bytes(uint8_t address, uint8_t reg, uint16_t bytes, SemaphoreHandle_t mutex);

#endif