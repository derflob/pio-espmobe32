#ifndef __ESPM32_SENSORS_H__
#define __ESPM32_SENSORS_H__

#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "freertos/task.h"

#include "sensors/types.h"
#include "sensors/bme280.h"
#include "sensors/kx023.h"
#include "sensors/opt3001.h"
#include "sensors/pir.h"
#include "sensors/system.h"
#include "sensors/tmp117.h"
#include "sensors/wake.h"

#include "mqtt_client.h"

union sensor_data {
    sensor_bme280 bme280;
    sensor_kx023 kx023;
    sensor_opt3001 opt3001;
    sensor_pir pir;
    sensor_system system;
    sensor_tmp117 tmp117;
    sensor_wake wake;
};

struct sensor_handle {
    sensor_progress progress;

    sensor_definition sensor;

    struct task {
        TaskHandle_t handle;
    } task;

    struct i2c {
        SemaphoreHandle_t mutex;
    } i2c;
};

void init_sensors(sensor_handle *sensor_list[], SemaphoreHandle_t i2c_mutex);
void configure_sensors(sensor_handle *sensor_list[]);
void start_sensors(sensor_handle *sensor_list[]);
void publish_sensor_discoveries(sensor_handle *sensor_list[], esp_mqtt_client_handle_t mqtt);
bool publish_sensor_values(sensor_handle *sensor_list[], esp_mqtt_client_handle_t mqtt);
void register_mqtt_topic(sensor_handle *sensor_list[], esp_mqtt_client_handle_t mqtt);
void abort_sensors(sensor_handle *sensor_list[]);

#endif