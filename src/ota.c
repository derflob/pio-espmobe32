#include "esp_log.h"

#include "ota.h"
#include "mqtt.h"
#include "constants.h"

#include <cJSON.h>

#include "esp_http_client.h"

#include "esp_partition.h"
#include "esp_ota_ops.h"

#define OTA_BUFFER_SIZE 1024
#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))

static char *TAG = "espmobe32-ota";

static bool _ota_in_progress = false;
static bool _ota_performed = false;

static void _mqtt_event_handler(void* event_handler_arg, esp_event_base_t event_base, int32_t event_id, void* event_data);

static void start_ota_task(const char *url);

static void ota_task(void *pvParameters);

static esp_http_client_handle_t connect_to_url(const char *url);
static void write_update_to_partition(const esp_http_client_handle_t client, const esp_partition_t *update_partition);

void start_ota_mqtt(esp_mqtt_client_handle_t mqtt)
{
    char ota_topic[MAX_MQTT_TOPIC_LENGTH] = { '\0' };
    get_unique_topic("ota", "update", ota_topic, MAX_MQTT_TOPIC_LENGTH);

    esp_mqtt_client_subscribe(mqtt, ota_topic, 1);
    esp_mqtt_client_register_event(mqtt, MQTT_EVENT_DATA, _mqtt_event_handler, NULL);
}

static void _mqtt_event_handler(void* event_handler_arg, esp_event_base_t event_base, int32_t event_id, void* event_data)
{
    esp_mqtt_event_handle_t event = event_data;
    esp_mqtt_client_handle_t mqtt = event->client;

    if (event_id != MQTT_EVENT_DATA)
        return;
    
    char ota_topic[MAX_MQTT_TOPIC_LENGTH] = { '\0' };
    get_unique_topic("ota", "update", ota_topic, MAX_MQTT_TOPIC_LENGTH);

    size_t topic_length = MIN(strlen(ota_topic), event->topic_len);

    if (strncmp(ota_topic, event->topic, topic_length))
        return;
    
    ESP_LOGI(TAG, "Got OTA message via MQTT!");
    
    cJSON *update_message = cJSON_Parse(event->data);

    cJSON *version = cJSON_GetObjectItemCaseSensitive(update_message, "version");
    cJSON *url = cJSON_GetObjectItemCaseSensitive(update_message, "url");

    size_t version_length = MIN(strlen(VERSION), strlen(version->valuestring));

    if (version && cJSON_IsString(version) && strncmp(VERSION, version->valuestring, version_length)) {
        if (url && cJSON_IsString(url)) {
            ESP_LOGI(TAG, "Got OTA version %s @ %s", version->valuestring, url->valuestring);
            start_ota_task(url->valuestring);
        }
    }

    cJSON_Delete(update_message);
    esp_mqtt_client_unsubscribe(mqtt, ota_topic);
    esp_mqtt_client_publish(mqtt, ota_topic, NULL, 0, 2, true);
}

void start_ota_task(const char* url) {
    _ota_in_progress = true;

    const char *task_data = strdup(url);

    xTaskCreatePinnedToCore(ota_task, TAG, 4096, (void * const)task_data, 5, NULL, APP_CPU_NUM);
}


static void ota_task(void *pvParameters)
{
    ESP_LOGI(TAG, "Starting OTA task.");

    const char *url = (char *)pvParameters;

    esp_http_client_handle_t client = connect_to_url(url);
    if (!client)
        vTaskDelete(NULL);
    
    const esp_partition_t *update_partition = esp_ota_get_next_update_partition(NULL);
    if (update_partition) {
        ESP_LOGI(TAG, "Writing to partition subtype %d at offset 0x%x", update_partition->subtype, update_partition->address);

        write_update_to_partition(client, update_partition);
    }
    
    esp_http_client_close(client);
    esp_http_client_cleanup(client);

    _ota_in_progress = false;

    vTaskDelete(NULL);
}

static esp_http_client_handle_t connect_to_url(const char *url) {
    esp_err_t error;

    esp_http_client_config_t config = {
        .url = url,
        .keep_alive_enable = true,
        .use_global_ca_store = true,
    };

    esp_http_client_handle_t client = esp_http_client_init(&config);
    if (!client) {
        ESP_LOGE(TAG, "HTTP init for OTA failed.");
        return NULL;
    }

    error = esp_http_client_open(client, 0);
    if (error != ESP_OK) {
        ESP_LOGE(TAG, "Failed to connect to OTA server.");
        esp_http_client_cleanup(client);
        return NULL;
    }

    esp_http_client_fetch_headers(client);

    return client;
}

static void write_update_to_partition(const esp_http_client_handle_t client, const esp_partition_t *update_partition)
{
    /* 
        adapted from esp-idf examples
        https://github.com/espressif/esp-idf/blob/master/examples/system/ota/native_ota_example/main/native_ota_example.c
    */

    esp_err_t error;

    const esp_partition_t *running_partition = esp_ota_get_running_partition();
    char ota_write_data[OTA_BUFFER_SIZE + 1] = {'\0'}; 
    esp_ota_handle_t update_handle = 0;

    int binary_file_length = 0;
    bool image_header_was_checked = false;

    while (1) {
        int data_read = esp_http_client_read(client, ota_write_data, OTA_BUFFER_SIZE);

        if (data_read < 0) {
            ESP_LOGE(TAG, "Error: SSL data read error");
            return;

        } else if (data_read > 0) {
            if (!image_header_was_checked) {
                esp_app_desc_t new_app_info;

                if (data_read > sizeof(esp_image_header_t) + sizeof(esp_image_segment_header_t) + sizeof(esp_app_desc_t)) {
                    // check current version with downloading
                    memcpy(&new_app_info, &ota_write_data[sizeof(esp_image_header_t) + sizeof(esp_image_segment_header_t)], sizeof(esp_app_desc_t));
                    ESP_LOGI(TAG, "New firmware version: %s", new_app_info.version);

                    esp_app_desc_t running_app_info;
                    if (esp_ota_get_partition_description(running_partition, &running_app_info) == ESP_OK) {
                        ESP_LOGI(TAG, "Running firmware version: %s", running_app_info.version);
                    }

                    const esp_partition_t *last_invalid_app = esp_ota_get_last_invalid_partition();
                    esp_app_desc_t invalid_app_info;
                    if (esp_ota_get_partition_description(last_invalid_app, &invalid_app_info) == ESP_OK) {
                        ESP_LOGI(TAG, "Last invalid firmware version: %s", invalid_app_info.version);
                    }

                    // check current version with last invalid partition
                    if (last_invalid_app != NULL) {
                        if (memcmp(invalid_app_info.version, new_app_info.version, sizeof(new_app_info.version)) == 0) {
                            ESP_LOGW(TAG, "New version is the same as invalid version.");
                            ESP_LOGW(TAG, "Previously, there was an attempt to launch the firmware with %s version, but it failed.", invalid_app_info.version);
                            ESP_LOGW(TAG, "The firmware has been rolled back to the previous version.");
                        }
                    }

                    image_header_was_checked = true;

                    error = esp_ota_begin(update_partition, OTA_SIZE_UNKNOWN, &update_handle);
                    if (error != ESP_OK) {
                        ESP_LOGE(TAG, "esp_ota_begin failed (%s)", esp_err_to_name(error));
                        return;
                    }
                    ESP_LOGI(TAG, "esp_ota_begin succeeded");

                } else {
                    ESP_LOGE(TAG, "received package is not fit len");
                }
            }

            error = esp_ota_write(update_handle, (const void *)ota_write_data, data_read);
            if (error != ESP_OK) {
                return;
            }

            binary_file_length += data_read;
            ESP_LOGD(TAG, "Written image length %d", binary_file_length);

        } else if (data_read == 0) {
           /*
            * As esp_http_client_read never returns negative error code, we rely on
            * `errno` to check for underlying transport connectivity closure if any
            */
            if (errno == ECONNRESET || errno == ENOTCONN) {
                ESP_LOGE(TAG, "Connection closed, errno = %d", errno);
                break;
            }
            if (esp_http_client_is_complete_data_received(client)) {
                ESP_LOGI(TAG, "Connection closed");
                break;
            }
        }
    }

    ESP_LOGI(TAG, "Total Write binary data length: %d", binary_file_length);
    if (!esp_http_client_is_complete_data_received(client)) {
        ESP_LOGE(TAG, "Error in receiving complete file");
        return;
    }

    error = esp_ota_end(update_handle);
    if (error != ESP_OK) {
        if (error == ESP_ERR_OTA_VALIDATE_FAILED) {
            ESP_LOGE(TAG, "Image validation failed, image is corrupted");
        } else {
            ESP_LOGE(TAG, "esp_ota_end failed (%s)!", esp_err_to_name(error));
        }
        return;
    }

    error = esp_ota_set_boot_partition(update_partition);
    if (error != ESP_OK) {
        ESP_LOGE(TAG, "esp_ota_set_boot_partition failed (%s)!", esp_err_to_name(error));
        return;
    }

    _ota_performed = true;
}

bool is_ota_in_progress() { return _ota_in_progress; }
bool is_ota_performed() { return _ota_performed; }
