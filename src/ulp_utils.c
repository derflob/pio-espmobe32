#include "esp_sleep.h"
#include "esp_err.h"
#include "esp32/ulp.h"

#include "sdkconfig.h"

#include "ulp_utils.h"
#include "ulp_main.h"


extern const uint8_t ulp_main_bin_start[] asm("_binary_ulp_main_bin_start");
extern const uint8_t ulp_main_bin_end[] asm("_binary_ulp_main_bin_end");

void init_ulp_periphery()
{
    ESP_ERROR_CHECK(ulp_load_binary(0, ulp_main_bin_start, (ulp_main_bin_end - ulp_main_bin_start) / sizeof(uint32_t)));
}

void start_ulp_program(uint32_t *ulp_entry)
{
    ulp_set_wakeup_period(0, CONFIG_ESPMOBE32_ULP_TICK_MS * 1000);

    ESP_ERROR_CHECK(ulp_run((ulp_entry - RTC_SLOW_MEM) / sizeof(uint32_t)));
    ESP_ERROR_CHECK(esp_sleep_enable_ulp_wakeup());
}
