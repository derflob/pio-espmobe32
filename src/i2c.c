#include "i2c.h"

#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "freertos/task.h"

#include "driver/i2c.h"

#include "pins.h"
#include "constants.h"

void grab_i2c_bus(SemaphoreHandle_t mutex)
{
    while (xSemaphoreTake(mutex, 0) == pdFALSE) {
        taskYIELD();
    };
}

void release_i2c_bus(SemaphoreHandle_t mutex)
{
    xSemaphoreGive(mutex);
}

SemaphoreHandle_t init_i2c_bus(void)
{
    const i2c_config_t config = {
        .mode = I2C_MODE_MASTER,
        .sda_io_num = SENSORS_SDA,
        .scl_io_num = SENSORS_SCL,
        .sda_pullup_en = false,
        .scl_pullup_en = false,
        .master = {
            .clk_speed = SENSOR_I2C_FREQ
        }
    };

    i2c_param_config(SENSOR_I2C_PORT, &config);

    i2c_driver_install(SENSOR_I2C_PORT, I2C_MODE_MASTER, 0, 0, ESP_INTR_FLAG_IRAM);

    return xSemaphoreCreateMutex();
}

void read_i2c_buffer(uint8_t address, uint8_t reg, uint8_t *buffer, size_t length, SemaphoreHandle_t mutex)
{
    grab_i2c_bus(mutex);

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();

    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (address << 1) | I2C_MASTER_WRITE, true);
    i2c_master_write_byte(cmd, reg, true);

    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (address << 1) | I2C_MASTER_READ, true);
    i2c_master_read(cmd, buffer, length, I2C_MASTER_LAST_NACK);
    i2c_master_stop(cmd);

    ESP_ERROR_CHECK_WITHOUT_ABORT(i2c_master_cmd_begin(SENSOR_I2C_PORT, cmd, pdMS_TO_TICKS(SENSOR_I2C_TIMEOUT)));

    i2c_cmd_link_delete(cmd);
    release_i2c_bus(mutex);
}

void write_i2c_buffer(uint8_t address, uint8_t reg, uint8_t *buffer, size_t length, SemaphoreHandle_t mutex)
{
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();

    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (address << 1) | I2C_MASTER_WRITE, true);
    i2c_master_write_byte(cmd, reg, true);
    i2c_master_write(cmd, buffer, length, true);
    i2c_master_stop(cmd);

    grab_i2c_bus(mutex);

    ESP_ERROR_CHECK_WITHOUT_ABORT(i2c_master_cmd_begin(SENSOR_I2C_PORT, cmd, pdMS_TO_TICKS(SENSOR_I2C_TIMEOUT)));

    i2c_cmd_link_delete(cmd);
    release_i2c_bus(mutex);
}

uint8_t read_i2c_byte(uint8_t address, uint8_t reg, SemaphoreHandle_t mutex)
{
    uint8_t byte;
    read_i2c_buffer(address, reg, &byte, 1, mutex);
    return byte;
}

void write_i2c_byte(uint8_t address, uint8_t reg, uint8_t byte, SemaphoreHandle_t mutex)
{
    write_i2c_buffer(address, reg, &byte, 1, mutex);
}

uint16_t read_i2c_2bytes(uint8_t address, uint8_t reg, SemaphoreHandle_t mutex)
{
    uint8_t buffer[2];
    read_i2c_buffer(address, reg, buffer, 2, mutex);
    return (((uint16_t)buffer[0]) << 8) | buffer[1];
}

void write_i2c_2bytes(uint8_t address, uint8_t reg, uint16_t bytes, SemaphoreHandle_t mutex)
{
    uint8_t buffer[2] = {(uint8_t)(bytes >> 8), (uint8_t)bytes};
    
    write_i2c_buffer(address, reg, buffer, 2, mutex);
}