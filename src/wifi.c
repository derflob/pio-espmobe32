#include <stdio.h>
#include <string.h>
#include <esp_wifi.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_log.h"
#include "esp_netif.h"

#include "wifi.h"
#include <wifi_manager.h>

static const char *TAG = "espmobe32-wifi";

static bool wifi_connected = false;
static bool wifi_ap_open = false;

static void wm_event_sta_got_ip_cb(void *pvParameters) { ESP_LOGI(TAG, "Wifi connected."); wifi_connected = true; };
static void wm_event_sta_disconnected_cb(void *pvParameters) { wifi_connected = false; };
static void wm_order_start_ap_cb(void *pvParameters) { wifi_ap_open = true; };
static void wm_order_stop_ap_cb(void *pvParameters) { wifi_ap_open = false; };

static void wm_order_connect_sta(void *pvParameters)
{
    esp_netif_t *sta = wifi_manager_get_esp_netif_sta();
    esp_netif_set_hostname(sta, CONFIG_ESPMOBE32_HOSTNAME);
}

static void start_wifi_manager_task(void *pvParemeters)
{
    ESP_LOGI(TAG, "Starting esp32-wifi-manager.");

    wifi_manager_start();

    wifi_manager_set_callback(WM_EVENT_STA_GOT_IP, &wm_event_sta_got_ip_cb);
    wifi_manager_set_callback(WM_EVENT_STA_DISCONNECTED, &wm_event_sta_disconnected_cb);

    wifi_manager_set_callback(WM_ORDER_CONNECT_STA, &wm_order_connect_sta);

    wifi_manager_set_callback(WM_ORDER_START_AP, wm_order_start_ap_cb);
    wifi_manager_set_callback(WM_ORDER_STOP_AP, wm_order_stop_ap_cb);

    vTaskDelete(NULL);
}

void start_wifi_manager(void)
{
    xTaskCreatePinnedToCore(&start_wifi_manager_task, "wifi-manager-start", 4096, NULL, WIFI_MANAGER_TASK_PRIORITY, NULL, PRO_CPU_NUM);
}

bool is_wifi_connected(void)
{
    return wifi_connected;
}

bool is_wifi_ap_open(void)
{
    return wifi_ap_open;
}
