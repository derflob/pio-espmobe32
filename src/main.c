#include <stdio.h>
#include "esp_sleep.h"
#include "esp_log.h"

#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"

#include "driver/rtc_io.h"

#include "state.h"

#include "constants.h"
#include "sdkconfig.h"
#include "pins.h"

#include "ulp_utils.h"

#include "led.h"

#include "i2c.h"

#include "sensors.h"
#include "sensors/types.h"

#include "esp_wifi.h"
#include "wifi.h"

#include "mqtt.h"
#include "mqtt_client.h"

#include "ota.h"

#include "nvs_flash.h"

static const char *TAG = "espmobe32";

static espmobe32_states state = ESPMOBE32_SETUP;

static sensor_handle *sensor_list[ESPMOBE32_SENSOR_MAX] = {};

static esp_mqtt_client_handle_t mqtt = NULL;

static bool _extended_boot = false;

void setup_nvs(void);

espmobe32_states process(void);
void start_deep_sleep();
void check_timeout();

void app_main(void)
{
    espmobe32_states new_state;

    ESP_LOGI(TAG, "This is %s (%s on esp-idf).", CONFIG_ESPMOBE32_HOSTNAME, VERSION);

    for (;;) {
        new_state = process();

        if (new_state != state) {
            ESP_LOGI(TAG, "State change: %u -> %u", state, new_state);

            state = new_state;

        } else {
            check_timeout();

            vTaskDelay(pdMS_TO_TICKS(50));
        }
    }
}

static espmobe32_states process_wakeup(void)
{
    ESP_LOGI(TAG, "Processing Wakup...");
    esp_sleep_wakeup_cause_t cause = esp_sleep_get_wakeup_cause();

    if (cause == ESP_SLEEP_WAKEUP_TIMER) {
        ESP_LOGI(TAG, "Periodic Wake.");

    } else if (cause == ESP_SLEEP_WAKEUP_ULP) {
        ESP_LOGI(TAG, "ULP Wake.");

    } else if (cause == ESP_SLEEP_WAKEUP_EXT1) {
        ESP_LOGI(TAG, "EXT1 Wake.");

    } else {
        ESP_LOGI(TAG, "Other Wake.");

        return ESPMOBE32_RESETTING;
    }

    return ESPMOBE32_CONNECT_WIFI;
}

static espmobe32_states process_publshing_data(void)
{
    bool done = publish_sensor_values(sensor_list, mqtt);

    if (done)
        return ESPMOBE32_PUBLISHED_DATA;
    else
        return state;
}

espmobe32_states process(void)
{
    if (state == ESPMOBE32_PROCESS_WAKEUP) {
        return process_wakeup();
    }

    if (state == ESPMOBE32_SETUP) {
        ESP_LOGI(TAG, "Setup espmobe32...");

        setup_nvs();

        start_status_led();

        SemaphoreHandle_t i2c_mutex = init_i2c_bus();

        init_sensors(sensor_list, i2c_mutex);

        return ESPMOBE32_PROCESS_WAKEUP;
    }
/*
    With esp-idf 4.3, starting the sensor readouts, while the WiFi
    is still initializing, leads to a crash, while the WiFi code reads
    from the NVS.

    Sensors are started once WiFi is connected instead, which didn't
    really worsen the overall time, until the espmobe32 is finished.

    if (state == ESPMOBE32_STARTING_MEASUREMENTS) {
        ESP_LOGI(TAG, "Start measurements...");

        start_sensors(sensor_list);

        return ESPMOBE32_WAIT_FOR_WIFI;
    }
*/
    if (state == ESPMOBE32_CONNECT_WIFI) {
        ESP_LOGI(TAG, "Start wifi...");

        start_wifi_manager();

        return ESPMOBE32_WAIT_FOR_WIFI;
    }

    if (state == ESPMOBE32_WAIT_FOR_WIFI) {
        if (is_wifi_connected()) {
            start_sensors(sensor_list);
            return ESPMOBE32_CONNECT_MQTT;
        }
    }

    if (state == ESPMOBE32_CONNECT_MQTT) {
        ESP_LOGI(TAG, "Start mqtt...");

        mqtt = start_mqtt();

        return ESPMOBE32_WAIT_FOR_MQTT;
    }

    if (state == ESPMOBE32_WAIT_FOR_MQTT) {
        if (is_mqtt_connected(mqtt)) {
            register_mqtt_topic(sensor_list, mqtt);

            start_ota_mqtt(mqtt);

            if (_extended_boot) {
                return ESPMOBE32_PUBLISHING_CONFIG;
            } else {
                return ESPMOBE32_PUBLISHING_DATA;
            }
        }
    }

    if (state == ESPMOBE32_PUBLISHING_CONFIG) {
        ESP_LOGI(TAG, "Publishing config...");

        publish_sensor_discoveries(sensor_list, mqtt);

        return ESPMOBE32_PUBLISHED_CONFIG;
    }

    if (state == ESPMOBE32_PUBLISHED_CONFIG) {
        ESP_LOGI(TAG, "Published config...");

        return ESPMOBE32_PUBLISHING_DATA;
    }

    if (state == ESPMOBE32_PUBLISHING_DATA) {
        return process_publshing_data();
    }

    if (state == ESPMOBE32_PUBLISHED_DATA) {
        int size = esp_mqtt_client_get_outbox_size(mqtt);
        if (size) {
            ESP_LOGI(TAG, "Waiting for MQTT outbox to clear (%i to go).", size);

            return state;
        }
        return ESPMOBE32_DISCONNECT_MQTT;
    }

    if (state == ESPMOBE32_DISCONNECT_MQTT) {
        ESP_LOGI(TAG, "Published data. Disconnecting mqtt...");

        esp_mqtt_client_disconnect(mqtt);
        esp_mqtt_client_stop(mqtt);

        return ESPMOBE32_DISCONNECT_WIFI;
    }

    if (state == ESPMOBE32_DISCONNECT_WIFI) {
        if (is_ota_in_progress())
            return state;

        esp_wifi_disconnect();
        return ESPMOBE32_DONE;
    }

    if (state == ESPMOBE32_RESETTING) {
        ESP_LOGI(TAG, "Resetting espmobe32...");

        init_ulp_periphery();

        configure_sensors(sensor_list);

        _extended_boot = true;

        return ESPMOBE32_CONNECT_WIFI;
    }

    if (state == ESPMOBE32_ABORT) {
        abort_sensors(sensor_list);

        if (is_mqtt_connected()) {
            esp_mqtt_client_disconnect(mqtt);
            esp_mqtt_client_stop(mqtt);
        }

        if (is_wifi_connected()) {
            esp_wifi_disconnect();
        }

        return ESPMOBE32_DONE;
    }

    if (state == ESPMOBE32_DONE) {
        stop_status_led();

        if (is_ota_performed()) {
            ESP_LOGW(TAG, "Restart after OTA!");
            esp_restart();
        } else {
            start_deep_sleep();
        }
    }

    return state;
}

void start_deep_sleep()
{
    ESP_LOGI(TAG, "start ulp");
    start_ulp_program();

    ESP_LOGI(TAG, "going to sleep.");
    gpio_deep_sleep_hold_en();
    esp_sleep_enable_timer_wakeup(CONFIG_ESPMOBE32_PERIODIC_WAKEUP_MS * 1000);
    esp_sleep_enable_ext1_wakeup((1ull << KX023_INTERRUPT), ESP_EXT1_WAKEUP_ANY_HIGH);
    esp_deep_sleep_start();
}

void setup_nvs(void) {
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        // OTA app partition table has a smaller NVS partition size than the non-OTA
        // partition table. This size mismatch may cause NVS initialization to fail.
        // If this happens, we erase NVS partition and initialize NVS again.
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK( err );
}

void check_timeout(void) {
    if (state == ESPMOBE32_ABORT || state == ESPMOBE32_DONE)
        return;

    if (is_wifi_ap_open() || is_ota_in_progress())
        return;

    if (esp_log_timestamp() > CONFIG_ESPMOBE32_SLOW_RUN_TIMEOUT) {
        ESP_LOGW(TAG, "Stuck for more than %u ms in state %u. Aborting!", CONFIG_ESPMOBE32_SLOW_RUN_TIMEOUT, state);

        state = ESPMOBE32_ABORT;
    }
}