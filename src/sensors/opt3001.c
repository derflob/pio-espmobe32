#include "sensors/opt3001.h"
#include "sensors.h"

#include <string.h>

#include "esp_log.h"

#include "constants.h"

#include "i2c.h"

#include "mqtt.h"

static const char *TAG = "sensor_opt3001";

static void start_singleshot_measurement(sensor_handle *handle)
{
    opt3001_register_configuration config = {};
    size_t config_size = sizeof(opt3001_register_configuration);

    config.conversion_mode = OPT3001_CONVERSION_MODE_SINGLESHOT;
    config.conversion_time = OPT3001_CONVERSION_TIME_100MS;
    config.range = OPT3001_RANGE_AUTO;
    config.latch = true;

    uint8_t *buffer = malloc(config_size);
    memcpy(buffer, &config, config_size);

    write_i2c_buffer(AMBIENT_LIGHT_SENSOR_ADDR, OPT3001_REG_CONFIGURATION, buffer, 2, handle->i2c.mutex);

    free(buffer);
}

static void sensor_opt3001_task(void *params)
{
    ESP_LOGI(TAG, "Starting measurement task.");

    sensor_handle *handle = (sensor_handle *)params;
    sensor_opt3001 *data = (sensor_opt3001 *)handle->sensor.data;

    handle->progress = ESPMOBE32_SP_MEASURING;

    start_singleshot_measurement(handle);

    opt3001_register_configuration config = {};
    size_t config_size = sizeof(opt3001_register_configuration);
    uint8_t *buffer = malloc(config_size);

    for (;;) {
        // max conversation time of 110 ms
        vTaskDelay(pdMS_TO_TICKS(125));

        read_i2c_buffer(AMBIENT_LIGHT_SENSOR_ADDR, OPT3001_REG_CONFIGURATION, buffer, 2, handle->i2c.mutex);
        memcpy(&config, buffer, config_size);
        ESP_LOGD(TAG, "Current configuration reg: %04x", *(uint16_t *)buffer);

        if (!config.conversion_ready) {
            ESP_LOGE(TAG, "Measurement timeout, restarting singleshot measurement.");
            start_singleshot_measurement(handle);

        } else {
            break;
        }
    }

    uint16_t result_data = read_i2c_2bytes(AMBIENT_LIGHT_SENSOR_ADDR, OPT3001_REG_RESULT, handle->i2c.mutex);
    opt3001_register_result result = {};
    memcpy(&result, &result_data, sizeof(uint16_t));

    ESP_LOGD(TAG, "%04x, exponent: %u, fractional: %i, range: %02x", result_data, result.exponent, result.fractional, config.range);

    float illuminance = 0.01 * (1 << result.exponent) * result.fractional;

    ESP_LOGI(TAG, "Got OPT3001 illuminance: %f", illuminance);

    free(buffer);

    data->illuminance = illuminance;
    handle->progress = ESPMOBE32_SP_MEASURED;

    vTaskDelete(NULL);
}

static void publish_discovery(sensor_handle *handle, esp_mqtt_client_handle_t mqtt)
{
    publish_discovery_sensor(mqtt, "Illuminance", "illuminance", "measurement", "lx", NULL, NULL, false);
}

static void publish_values(sensor_handle *handle, esp_mqtt_client_handle_t mqtt)
{
    publish_data_float(mqtt, "illuminance", handle->sensor.data->opt3001.illuminance, false);

    handle->progress = ESPMOBE32_SP_PUBLISHED;
}

void init_sensor_opt3001(sensor_definition *definition)
{
    definition->name = TAG;
    definition->task = &sensor_opt3001_task;
    definition->configure = NULL;
    definition->publish_discovery = &publish_discovery;
    definition->publish_values = &publish_values;

    definition->data = malloc(sizeof(sensor_opt3001));
}
