#include "sensors/tmp117.h"
#include "sensors.h"

#include <string.h>
#include <stdint.h>

#include "esp_log.h"

#include "constants.h"
#include "pins.h"

#include "i2c.h"

#include "mqtt.h"

static const char *TAG = "sensor_tmp117";

static void start_singleshot_measurement(sensor_handle *handle)
{
    tmp117_register_configuration config = {};
    size_t config_size = sizeof(tmp117_register_configuration);

    config.avg_mode = TMP117_AVERAGING_8x;
    config.conversion_mode = TMP117_CONVERSION_MODE_ONESHOT;

    uint8_t *buffer = malloc(config_size);
    memcpy(buffer, &config, config_size);

    ESP_LOGD(TAG, "Writing configuration.");
    write_i2c_buffer(TEMPERATURE_SENSOR_ADDR, TMP117_REG_CONFIGURATION, buffer, 2, handle->i2c.mutex);
    ESP_LOGD(TAG, "Singleshot measurement configured: %04x", *(uint16_t *)buffer);

    free(buffer);
}

static void sensor_tmp117_task(void *params)
{
    ESP_LOGI(TAG, "Starting measurement task.");

    sensor_handle *handle = (sensor_handle *)params;
    sensor_tmp117 *data = (sensor_tmp117 *)handle->sensor.data;

    handle->progress = ESPMOBE32_SP_MEASURING;

    tmp117_register_configuration config = {};
    size_t config_size = sizeof(tmp117_register_configuration);
    uint8_t *buffer = malloc(config_size);

    start_singleshot_measurement(handle);

    for (;;) {
        // max single shot conversion time of 8 (avg) * 17.5 ms
        vTaskDelay(pdMS_TO_TICKS(8 * 18));

        read_i2c_buffer(TEMPERATURE_SENSOR_ADDR, TMP117_REG_CONFIGURATION, buffer, 2, handle->i2c.mutex);
        memcpy(&config, buffer, config_size);
        ESP_LOGD(TAG, "Current configuration reg: %04x", *(uint16_t *)buffer);

        if (!config.data_ready) {
            ESP_LOGE(TAG, "Measurement timeout, restarting singleshot measurement.");
            start_singleshot_measurement(handle);

        } else {
            break;
        }
    }

    ESP_LOGD(TAG, "Measurement available.");

    int16_t temperature_data = (int16_t)read_i2c_2bytes(TEMPERATURE_SENSOR_ADDR, TMP117_REG_TEMP_RESULT, handle->i2c.mutex);

    float temperature = (float)temperature_data * TMP117_TEMPERATURE_FACTOR;

    ESP_LOGI(TAG, "Got TMP117 temperature: %f (%04x)", temperature, temperature_data);

    free(buffer);

    data->temperature = temperature;
    handle->progress = ESPMOBE32_SP_MEASURED;

    vTaskDelete(NULL);
}

static void publish_discovery(sensor_handle *handle, esp_mqtt_client_handle_t mqtt)
{
    publish_discovery_sensor(mqtt, "Temperature", "temperature", "measurement", "°C", NULL, NULL, false);
}

static void publish_values(sensor_handle *handle, esp_mqtt_client_handle_t mqtt)
{
    publish_data_float(mqtt, "temperature", handle->sensor.data->tmp117.temperature, false);

    handle->progress = ESPMOBE32_SP_PUBLISHED;
}

void init_sensor_tmp117(sensor_definition *definition)
{
    definition->name = TAG;
    definition->task = &sensor_tmp117_task;
    definition->configure = NULL;
    definition->publish_discovery = &publish_discovery;
    definition->publish_values = &publish_values;

    definition->data = malloc(sizeof(sensor_tmp117));
}
