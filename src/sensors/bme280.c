#include "sensors/bme280.h"
#include "sensors.h"

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include <bme280.h>

#include <string.h>

#include "esp_log.h"

#include "constants.h"

#include "i2c.h"

#include "mqtt.h"

static const char *TAG = "sensor_bme280";

static void user_delay_us(uint32_t period, void *intf_ptr)
{
    vTaskDelay(pdMS_TO_TICKS(period) / 1000);
}

static int8_t user_i2c_read(uint8_t reg_addr, uint8_t *reg_data, uint32_t len, void *intf_ptr)
{
    read_i2c_buffer(ENVIRONMENTAL_SENSOR_ADDR, reg_addr, reg_data, len, (SemaphoreHandle_t)intf_ptr);

    return 0;
}

static int8_t user_i2c_write(uint8_t reg_addr, const uint8_t *reg_data, uint32_t len, void *intf_ptr)
{
    write_i2c_buffer(ENVIRONMENTAL_SENSOR_ADDR, reg_addr, (uint8_t *)reg_data, len, (SemaphoreHandle_t)intf_ptr);

    return 0;
}

static void setup_sensor_device(struct bme280_dev *device, SemaphoreHandle_t i2c_mutex)
{
    device->intf_ptr = (void *)i2c_mutex;
    device->intf = BME280_I2C_INTF;
    device->read = user_i2c_read;
    device->write = user_i2c_write;
    device->delay_us = user_delay_us;

    bme280_init(device);
}

static void start_singleshot_measurement(struct bme280_dev *device)
{
    struct bme280_settings settings;
    settings.osr_t = BME280_OVERSAMPLING_2X;
    settings.osr_h = BME280_OVERSAMPLING_1X;
    settings.osr_p = BME280_OVERSAMPLING_16X;
    settings.filter = BME280_FILTER_COEFF_OFF;

    bme280_set_sensor_settings(
        BME280_SEL_OSR_TEMP | BME280_SEL_OSR_HUM | BME280_SEL_OSR_PRESS | BME280_SEL_FILTER,
        &settings,
        device
    );

    bme280_set_sensor_mode(BME280_POWERMODE_FORCED, device);

    ESP_LOGI(TAG, "Started BME280 measurement.");
}

static void sensor_bme280_task(void *params)
{
    ESP_LOGI(TAG, "Starting measurement task.");

    sensor_handle *handle = (sensor_handle *)params;
    sensor_bme280 *data = (sensor_bme280 *)handle->sensor.data;

    struct bme280_dev device = {};

    handle->progress = ESPMOBE32_SP_MEASURING;

    setup_sensor_device(&device, handle->i2c.mutex);

    start_singleshot_measurement(&device);

    struct bme280_data measurements;

    for (uint8_t i = 1;; i++) {
        if (i == UINT8_MAX) {
            ESP_LOGE(TAG, "Measurement timeout, restarting singleshot measurement.");
            start_singleshot_measurement(&device);
        }

        uint8_t mode;
        bme280_get_sensor_mode(&mode, &device);

        if (mode != BME280_POWERMODE_SLEEP) {
            ESP_LOGI(TAG, "Measuring BME280 measurement.");
            taskYIELD();
            continue;
        }

        uint8_t status = read_i2c_byte(ENVIRONMENTAL_SENSOR_ADDR, BME280_REG_STATUS, (SemaphoreHandle_t)device.intf_ptr);
        if (status & (1 << 3)) {
            ESP_LOGI(TAG, "Converting BME280 measurement.");
            taskYIELD();
            continue;
        }

        device.delay_us(15000, NULL);

        ESP_LOGI(TAG, "Getting BME280 measurement.");
        int8_t error = bme280_get_sensor_data(BME280_ALL, &measurements, &device);

        if (error == BME280_OK)
            break;

        ESP_LOGD(TAG, "Measurement not yet done.");

        taskYIELD();
    }
    ESP_LOGI(TAG, "Got BME280 measurements: %fC, %frH, %fPa", measurements.temperature, measurements.humidity, measurements.pressure);

    data->temperature = measurements.temperature;
    data->humidity = measurements.humidity;
    data->pressure = measurements.pressure / 100.0;

    handle->progress = ESPMOBE32_SP_MEASURED;

    vTaskDelete(NULL);
}

static void publish_discovery(sensor_handle *handle, esp_mqtt_client_handle_t mqtt)
{
    publish_discovery_sensor(mqtt, "Humidity", "humidity", "measurement", "%", NULL, "bme280", false);
    publish_discovery_sensor(mqtt, "Pressure", "pressure", "measurement", "hPa", NULL, "bme280", false);
}

static void publish_values(sensor_handle *handle, esp_mqtt_client_handle_t mqtt)
{
    publish_data_float(mqtt, "humidity", handle->sensor.data->bme280.humidity, false);
    publish_data_float(mqtt, "pressure", handle->sensor.data->bme280.pressure, false);

    cJSON *attributes = cJSON_CreateObject();

    char temperature_string[8] = {'\0'};
    snprintf(temperature_string, 8, "%.2f", handle->sensor.data->bme280.temperature);
    cJSON_AddRawToObject(attributes, "temperature", temperature_string);

    publish_data_attributes(mqtt, "bme280", attributes, false);

    cJSON_Delete(attributes);

    handle->progress = ESPMOBE32_SP_PUBLISHED;
}

void init_sensor_bme280(sensor_definition *definition)
{
    definition->name = TAG;
    definition->task = &sensor_bme280_task;
    definition->configure = NULL;
    definition->publish_discovery = &publish_discovery;
    definition->publish_values = &publish_values;

    definition->data = malloc(sizeof(sensor_bme280));
}