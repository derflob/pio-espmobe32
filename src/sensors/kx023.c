#include "sensors/kx023.h"
#include "sensors.h"

#include <string.h>
#include <stdint.h>

#include "driver/rtc_io.h"
#include "pins.h"

#include "esp_log.h"

#include "sdkconfig.h"
#include "constants.h"
#include "pins.h"

#include "i2c.h"

#include "mqtt.h"

static const char *TAG = "sensor_kx023";

void sensor_kx023_task(void *params)
{
    ESP_LOGI(TAG, "Starting measurement task.");

    sensor_handle *handle = (sensor_handle *)params;
    sensor_kx023 *data = (sensor_kx023 *)handle->sensor.data;

    handle->progress = ESPMOBE32_SP_MEASURING;

    uint8_t status[5] = {};

    read_i2c_buffer(ACCELERATION_SENSOR_ADDR, KX023_REG_INS1, status, 5, handle->i2c.mutex);

    const uint8_t ins2 = status[1];
    const uint8_t tap = (ins2 & 0xC) >> 2;

    if (tap == 0x01)
        ESP_LOGI(TAG, "Single tapped.");

    if (tap == 0x02)
        ESP_LOGI(TAG, "Double tapped.");

    data->single_tap = (tap == 0x01);
    data->double_tap = (tap == 0x02);

    handle->progress = ESPMOBE32_SP_MEASURED;

    vTaskDelete(NULL);
}

void configure_sensor_kx023(sensor_handle *handle)
{
    ESP_LOGI(TAG, "Configuring sensor.");

    rtc_gpio_init(KX023_INTERRUPT);
    rtc_gpio_set_direction(KX023_INTERRUPT, RTC_GPIO_MODE_INPUT_ONLY);
    rtc_gpio_pullup_dis(KX023_INTERRUPT);
    rtc_gpio_pulldown_en(KX023_INTERRUPT);

    uint8_t config[13] = {
        // start configuration
        // CNTL1 - 3
        0x00,
        0x3F,
        0x98,
        // ODCNTL
        0x20,
        // INC1 - 6
        0x30, // en PIN1 intr, active high
        0x3F,
        0x3F,
        0x04, // tap (4) motion (2) intr
        0x00,
        0x00,

        // TILT/WUFC
        0x00,
        0x00,

        // TDTRC
        0x03
    };

    write_i2c_buffer(ACCELERATION_SENSOR_ADDR, KX023_REG_CNTL1, config, 13, handle->i2c.mutex);

    vTaskDelay(pdMS_TO_TICKS(75));

    // activate operation
    write_i2c_byte(ACCELERATION_SENSOR_ADDR, KX023_REG_CNTL1, 0x84, handle->i2c.mutex);
}

static void publish_discovery(sensor_handle *handle, esp_mqtt_client_handle_t mqtt)
{
    publish_discovery_trigger(mqtt, "tapped", "single_tapped", "tapped", CONFIG_ESPMOBE32_HOSTNAME, SINGLE_TAP_PAYLOAD);
    publish_discovery_trigger(mqtt, "tapped", "double_tapped", "double tapped", CONFIG_ESPMOBE32_HOSTNAME, DOUBLE_TAP_PAYLOAD);
}

static void publish_values(sensor_handle *handle, esp_mqtt_client_handle_t mqtt)
{
    if (handle->sensor.data->kx023.single_tap)
        publish_trigger(mqtt, "tapped", SINGLE_TAP_PAYLOAD, false);

    if (handle->sensor.data->kx023.double_tap)
        publish_trigger(mqtt, "tapped", DOUBLE_TAP_PAYLOAD, false);

    handle->progress = ESPMOBE32_SP_PUBLISHED;
}

void init_sensor_kx023(sensor_definition *definition)
{
    definition->name = TAG;
    definition->task = &sensor_kx023_task;
    definition->configure = &configure_sensor_kx023;
    definition->publish_discovery = &publish_discovery;
    definition->publish_values = &publish_values;
    
    definition->data = malloc(sizeof(sensor_kx023));
}
