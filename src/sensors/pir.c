#include "sensors/pir.h"
#include "sensors.h"

#include <stdint.h>

#include "driver/rtc_io.h"
#include "pins.h"

#include "esp_log.h"

#include "ulp_utils.h"
#include "ulp_main.h"

#include "constants.h"

#include "mqtt.h"

static const char *TAG = "sensor_pir";

static void configure_sensor_pir(sensor_handle *handle)
{
    rtc_gpio_init(PIR_INPUT);
    rtc_gpio_set_direction(PIR_INPUT, RTC_GPIO_MODE_INPUT_ONLY);

    ulp_ticks_for_inactivity = CONFIG_ESPMOBE32_ULP_TICKS_CONTINOUS_INACTIVITY;
    ulp_ticks_for_activity = CONFIG_ESPMOBE32_ULP_TICKS_CONTINOUS_ACTIVITY;
    ulp_ticks = 1;
}

static void sensor_pir_task(void *params)
{
    ESP_LOGI(TAG, "Starting measurement task.");

    sensor_handle *handle = (sensor_handle *)params;
    sensor_pir *data = (sensor_pir *)handle->sensor.data;

    handle->progress = ESPMOBE32_SP_MEASURING;

    ESP_LOGI(TAG, "Got PIR status: %u", ulp_pir_status);

    data->movement = (uint16_t)ulp_pir_status;

    handle->progress = ESPMOBE32_SP_MEASURED;

    vTaskDelete(NULL);
}

static void publish_discovery(sensor_handle *handle, esp_mqtt_client_handle_t mqtt)
{
    publish_discovery_binary_sensor(mqtt, "Movement", "motion", NULL, PIR_PAYLOAD_ON, PIR_PAYLOAD_OFF, true);
}

static void publish_values(sensor_handle *handle, esp_mqtt_client_handle_t mqtt)
{
    bool movement = handle->sensor.data->pir.movement;
    publish_data_string(mqtt, "movement", movement ? PIR_PAYLOAD_ON : PIR_PAYLOAD_OFF, false);

    handle->progress = ESPMOBE32_SP_PUBLISHED;
}

void init_sensor_pir(sensor_definition *definition)
{
    definition->name = TAG;
    definition->task = &sensor_pir_task;
    definition->configure = &configure_sensor_pir;
    definition->publish_discovery = &publish_discovery;
    definition->publish_values = &publish_values;

    definition->data = malloc(sizeof(sensor_pir));
}
