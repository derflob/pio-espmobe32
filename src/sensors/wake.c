
#include "sensors/wake.h"
#include "sensors.h"

#include "esp_log.h"
#include "esp_sleep.h"

#include "sdkconfig.h"
#include "constants.h"

#include "mqtt.h"

static const char *TAG = "sensor_wake";

static void sensor_wake_task(void *params)
{
    ESP_LOGI(TAG, "Starting measurement task.");

    sensor_handle *handle = (sensor_handle *)params;
    sensor_wake *data = (sensor_wake *)handle->sensor.data;

    handle->progress = ESPMOBE32_SP_MEASURING;

    esp_sleep_wakeup_cause_t cause = esp_sleep_get_wakeup_cause();
    wake_reason reason = ESPMOBE32_WAKE_UNKNOWN;

    switch (cause) {
        case ESP_SLEEP_WAKEUP_TIMER:
            reason = ESPMOBE32_WAKE_PERIODIC;
            break;
        
        case ESP_SLEEP_WAKEUP_ULP:
            reason = ESPMOBE32_WAKE_ULP;
            break;
        
        case ESP_SLEEP_WAKEUP_EXT1:
            reason = ESPMOBE32_WAKE_EXT1;
            break;
        
        default:
            reason = ESPMOBE32_WAKE_UNKNOWN;
            break;
    }

    ESP_LOGI(TAG, "Got Wake reason: %i", reason);

    data->wake_reason = reason;
    handle->progress = ESPMOBE32_SP_MEASURED;

    vTaskDelete(NULL);
}

static void publish_discovery(sensor_handle *handle, esp_mqtt_client_handle_t mqtt)
{
    publish_discovery_trigger(mqtt, "wake", "wake_any", "woken", CONFIG_ESPMOBE32_HOSTNAME, WAKE_ANY_PAYLOAD);

    publish_discovery_trigger(mqtt, "wake", "wake_ulp", "woken by ULP", CONFIG_ESPMOBE32_HOSTNAME, WAKE_ULP_PAYLOAD);
    publish_discovery_trigger(mqtt, "wake", "wake_ext1", "woken by EXT1", CONFIG_ESPMOBE32_HOSTNAME, WAKE_EXT1_PAYLOAD);
    publish_discovery_trigger(mqtt, "wake", "wake_periodic", "woken periodically", CONFIG_ESPMOBE32_HOSTNAME, WAKE_PERIODIC_PAYLOAD);
    publish_discovery_trigger(mqtt, "wake", "wake_unknown", "woken by unknown", CONFIG_ESPMOBE32_HOSTNAME, WAKE_UNKNOWN_PAYLOAD);
}

static void publish_values(sensor_handle *handle, esp_mqtt_client_handle_t mqtt)
{
    publish_trigger(mqtt, "wake", WAKE_ANY_PAYLOAD, false);

    switch (handle->sensor.data->wake.wake_reason) {
        case ESPMOBE32_WAKE_EXT1:
            publish_trigger(mqtt, "wake", WAKE_EXT1_PAYLOAD, false);
            break;
        case ESPMOBE32_WAKE_PERIODIC:
            publish_trigger(mqtt, "wake", WAKE_PERIODIC_PAYLOAD, false);
            break;
        case ESPMOBE32_WAKE_ULP:
            publish_trigger(mqtt, "wake", WAKE_ULP_PAYLOAD, false);
            break;
        default:
            publish_trigger(mqtt, "wake", WAKE_UNKNOWN_PAYLOAD, false);
            break;
    }

    handle->progress = ESPMOBE32_SP_PUBLISHED;
}

void init_sensor_wake(sensor_definition *definition)
{
    definition->name = TAG;
    definition->task = &sensor_wake_task;
    definition->configure = NULL;
    definition->publish_discovery = &publish_discovery;
    definition->publish_values = &publish_values;

    definition->data = malloc(sizeof(sensor_wake));
}
