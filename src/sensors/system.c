#include "sensors/system.h"
#include "sensors.h"

#include <stdint.h>

#include "esp_log.h"
#include "driver/adc.h"
#include "esp_adc_cal.h"

#include "esp_timer.h"

#include "nvs.h"

#include "constants.h"
#include "pins.h"

#include "mqtt.h"

static const char *TAG = "sensor_system";
static RTC_DATA_ATTR unsigned long _reboots = 0;

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))

static void load_battery_settings(sensor_system *system);
static void publish_attributes(sensor_handle *handle, esp_mqtt_client_handle_t mqtt);

static void _register_mqtt_topic(sensor_handle *handle, esp_mqtt_client_handle_t mqtt);
static void mqtt_event_handler(void *event_handler_arg, esp_event_base_t event_base, int32_t event_id, void* event_data);
static void mqtt_reset_topic(esp_mqtt_client_handle_t mqtt, char *topic);
static void mqtt_extract_battery_sense_factor(sensor_handle *handle, cJSON *json);
static void mqtt_extract_battery_voltage_low(sensor_handle *handle, cJSON *json);

static void sensor_system_task(void *params)
{
    ESP_LOGI(TAG, "Starting measurement task.");

    float voltage;

    esp_err_t calibration_available = esp_adc_cal_check_efuse(ESP_ADC_CAL_VAL_EFUSE_VREF);
    esp_adc_cal_characteristics_t characteristics = {};

    sensor_handle *handle = (sensor_handle *)params;
    sensor_system *data = (sensor_system *)handle->sensor.data;

    handle->progress = ESPMOBE32_SP_MEASURING;

    _reboots +=1;
    data->reboots = _reboots;

    adc_power_acquire();

    adc_gpio_init(ADC_UNIT_1, BATTERY_SENSE_ADC_CHANNEL);
    adc1_config_channel_atten(BATTERY_SENSE_ADC1_CHANNEL, ADC_ATTEN_DB_0);
    adc1_config_width(ADC_WIDTH_BIT_12);

    int raw_value = adc1_get_raw(BATTERY_SENSE_ADC1_CHANNEL);

    if (raw_value < 0) {
        handle->progress = ESPMOBE32_SP_FAILED;

    } else {
        if (calibration_available == ESP_OK) {
            ESP_LOGI(TAG, "Using eFuse vRef calibration.");
            esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_DB_0, ADC_WIDTH_BIT_12, 0, &characteristics);
            voltage = (float)esp_adc_cal_raw_to_voltage(raw_value, &characteristics) / 1000.0f;
        } else {
            voltage = (float)raw_value / 4096.0f;
        }

        voltage *= data->battery_sense_factor;

        ESP_LOGI(TAG, "System voltage: %f", voltage);

        data->battery_voltage = voltage;
        data->battery_low = voltage < data->battery_voltage_low;
        handle->progress = ESPMOBE32_SP_MEASURED;
    }

    adc_power_release();

    vTaskDelete(NULL);
}

static void publish_discovery(sensor_handle *handle, esp_mqtt_client_handle_t mqtt)
{
    publish_discovery_sensor(mqtt, "Voltage", "voltage", "measurement", "V", "mdi:flash", "battery", false);
    publish_discovery_binary_sensor(mqtt, "Charge", "battery", "battery", BATTERY_CHARGE_LOW_PAYLOAD, BATTERY_CHARGE_NORMAL_PAYLOAD, false);
    publish_discovery_sensor(mqtt, "Reboot", NULL, NULL, "n", "mdi:restart", NULL, true);
    publish_discovery_sensor(mqtt, "Runtime", "duration", "measurement", "ms", "mdi:timer-outline", NULL, true);
}

static void publish_values(sensor_handle *handle, esp_mqtt_client_handle_t mqtt)
{
    char *battery_charge_payload = handle->sensor.data->system.battery_low ? BATTERY_CHARGE_LOW_PAYLOAD : BATTERY_CHARGE_NORMAL_PAYLOAD;
    publish_data_float(mqtt, "voltage", handle->sensor.data->system.battery_voltage, false);
    publish_data_string(mqtt, "charge",  battery_charge_payload, false);
    publish_data_uint32(mqtt, "reboot", handle->sensor.data->system.reboots, false);
    publish_data_uint32(mqtt, "runtime", esp_log_timestamp(), false);

    publish_attributes(handle, mqtt);

    handle->progress = ESPMOBE32_SP_PUBLISHED;
}

static void publish_attributes(sensor_handle *handle, esp_mqtt_client_handle_t mqtt)
{
    cJSON *attributes = cJSON_CreateObject();

    cJSON_AddNumberToObject(attributes, "sense_factor", handle->sensor.data->system.battery_sense_factor);
    cJSON_AddNumberToObject(attributes, "voltage_low", handle->sensor.data->system.battery_voltage_low);

    publish_data_attributes(mqtt, "battery", attributes, false);

    cJSON_Delete(attributes);
}

void init_sensor_system(sensor_definition *definition)
{
    definition->name = TAG;
    definition->task = &sensor_system_task;
    definition->configure = NULL;
    definition->publish_discovery = &publish_discovery;
    definition->publish_values = &publish_values;
    definition->register_mqtt_topic = &_register_mqtt_topic;

    definition->data = malloc(sizeof(sensor_system));

    load_battery_settings(&definition->data->system);
}

static void load_battery_settings(sensor_system *system)
{
    nvs_handle_t nvs;
    esp_err_t err;

    system->battery_sense_factor = BATTERY_SENSE_FACTOR;
    system->battery_voltage_low = (float)CONFIG_ESPMOBE32_BATTERY_VOLTAGE_LOW / 1000.0f;

    err = nvs_open("sensor_system", NVS_READONLY, &nvs);
    if (err != ESP_OK) {
        return;
    }

    uint16_t battery_sense_factor_mV = 0;

    err = nvs_get_u16(nvs, "bt_sense_fctr", &battery_sense_factor_mV);
    if (err == ESP_OK) {
        system->battery_sense_factor = (float)battery_sense_factor_mV / 1000.0f;
    }

    uint16_t battery_voltage_low_mV = 0;

    err = nvs_get_u16(nvs, "bt_voltage_low", &battery_voltage_low_mV);
    if (err == ESP_OK) {
        system->battery_voltage_low = (float)battery_voltage_low_mV / 1000.0f;
    }

    nvs_close(nvs);
}

static void save_battery_sense_factor(sensor_system *system, float factor)
{
    nvs_handle_t nvs;
    esp_err_t err;

    err = nvs_open("sensor_system", NVS_READWRITE, &nvs);
    if (err != ESP_OK) {
        return;
    }

    if (factor == 0.0f) {
        nvs_erase_key(nvs, "bt_sense_fctr");
    } else {
        uint16_t battery_sense_factor_mV = (uint16_t)(factor * 1000.0f);
        nvs_set_u16(nvs, "bt_sense_fctr", battery_sense_factor_mV);
    }

    nvs_commit(nvs);
    nvs_close(nvs);
}

static void save_battery_voltage_low(sensor_system *system, float voltage)
{
    nvs_handle_t nvs;
    esp_err_t err;

    err = nvs_open("sensor_system", NVS_READWRITE, &nvs);
    if (err != ESP_OK) {
        return;
    }

    if (voltage == 0.0f) {
        nvs_erase_key(nvs, "bt_voltage_low");
    } else {
        uint16_t battery_voltage_low_mV = (uint16_t)(voltage * 1000.0f);
        nvs_set_u16(nvs, "bt_voltage_low", battery_voltage_low_mV);
    }

    nvs_commit(nvs);
    nvs_close(nvs);
}

static void _register_mqtt_topic(sensor_handle *handle, esp_mqtt_client_handle_t mqtt)
{
    char system_settings_topic[MAX_MQTT_TOPIC_LENGTH] = { '\0' };
    get_settings_topic("system", system_settings_topic, MAX_MQTT_TOPIC_LENGTH);

    esp_mqtt_client_subscribe(mqtt, system_settings_topic, 1);
    esp_mqtt_client_register_event(mqtt, MQTT_EVENT_DATA, mqtt_event_handler, (void *)handle);
}

static void mqtt_event_handler(void *event_handler_arg, esp_event_base_t event_base, int32_t event_id, void* event_data)
{
    sensor_handle *handle = (sensor_handle *)event_handler_arg;
    esp_mqtt_event_handle_t event = event_data;
    esp_mqtt_client_handle_t mqtt = event->client;

    if (event_id != MQTT_EVENT_DATA)
        return;

    char system_settings_topic[MAX_MQTT_TOPIC_LENGTH] = { '\0' };
    get_settings_topic("system", system_settings_topic, MAX_MQTT_TOPIC_LENGTH);

    size_t topic_length = MIN(strlen(system_settings_topic), event->topic_len);

    if (strncmp(system_settings_topic, event->topic, topic_length)) {
        return;
    }

    cJSON *json = cJSON_Parse(event->data);
    if (!json) {
        return;
    }

    mqtt_extract_battery_sense_factor(handle, json);
    mqtt_extract_battery_voltage_low(handle, json);

    mqtt_reset_topic(mqtt, system_settings_topic);

    cJSON_Delete(json);
}

static void mqtt_reset_topic(esp_mqtt_client_handle_t mqtt, char *topic)
{
    esp_mqtt_client_publish(mqtt, topic, NULL, 0, 2, true);
}

static void mqtt_extract_battery_sense_factor(sensor_handle *handle, cJSON *json)
{
    cJSON *sense_factor = cJSON_GetObjectItemCaseSensitive(json, "battery_sense_factor");
    if (!sense_factor) {
        return;
    }

    if (cJSON_IsNull(sense_factor)) {
        handle->sensor.data->system.battery_sense_factor = BATTERY_SENSE_FACTOR;
        save_battery_sense_factor(&handle->sensor.data->system, 0.0f);

    } else if (cJSON_IsNumber(sense_factor)) {
        handle->sensor.data->system.battery_sense_factor = sense_factor->valuedouble;
        save_battery_sense_factor(&handle->sensor.data->system, sense_factor->valuedouble);
    }
}

static void mqtt_extract_battery_voltage_low(sensor_handle *handle, cJSON *json)
{
    cJSON *battery_low = cJSON_GetObjectItemCaseSensitive(json, "battery_voltage_low");
    if (!battery_low) {
        return;
    }

    if (cJSON_IsNull(battery_low)) {
        handle->sensor.data->system.battery_voltage_low = (float)CONFIG_ESPMOBE32_BATTERY_VOLTAGE_LOW / 1000.0f;
        save_battery_voltage_low(&handle->sensor.data->system, 0.0f);

    } else if (cJSON_IsNumber(battery_low)) {
        handle->sensor.data->system.battery_voltage_low = battery_low->valuedouble;
        save_battery_voltage_low(&handle->sensor.data->system, battery_low->valuedouble);
    }
}
