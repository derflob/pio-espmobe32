#include "sensors.h"

#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "freertos/task.h"

#include "soc/soc.h"
#include "esp_log.h"

#include "constants.h"

static const char *TAG = "sensors";

void init_sensors(sensor_handle *sensor_list[], SemaphoreHandle_t i2c_mutex)
{
    sensor_handle *handle = NULL;

    for (sensor_types type = 0; type < ESPMOBE32_SENSOR_MAX; type++) {
        handle = (sensor_handle *)malloc(sizeof(sensor_handle));
        memset((void *)handle, 0, sizeof(sensor_handle));

        handle->i2c.mutex = i2c_mutex;
        handle->task.handle = NULL;

        handle->progress = ESPMOBE32_SP_INITIALIZING;

        switch (type) {
        case ESPMOBE32_SENSOR_SYSTEM:
            init_sensor_system(&handle->sensor);
            break;

        case ESPMOBE32_SENSOR_TMP117:
            init_sensor_tmp117(&handle->sensor);
            break;

        case ESPMOBE32_SENSOR_OPT3001:
            init_sensor_opt3001(&handle->sensor);
            break;

        case ESPMOBE32_SENSOR_KX023:
            init_sensor_kx023(&handle->sensor);
            break;

        case ESPMOBE32_SENSOR_BME280:
            init_sensor_bme280(&handle->sensor);
            break;

        case ESPMOBE32_SENSOR_PIR:
            init_sensor_pir(&handle->sensor);
            break;

        case ESPMOBE32_SENSOR_WAKE:
            init_sensor_wake(&handle->sensor);
            break;

        default:
            free(handle);
            handle = NULL;
            break;
        };

        sensor_list[type] = handle;
    }
}

void start_sensors(sensor_handle *sensor_list[])
{
    for (sensor_types type = 0; type < ESPMOBE32_SENSOR_MAX; type++) {
        if (!sensor_list[type])
            continue;

        sensor_handle *handle = sensor_list[type];

        if (handle->sensor.task) {
            xTaskCreatePinnedToCore(handle->sensor.task, handle->sensor.name, 4096, (void *)handle, SENSOR_TASK_PRIORITY, &handle->task.handle, APP_CPU_NUM);
        }
    }
}

void configure_sensors(sensor_handle *sensor_list[])
{
    ESP_LOGI(TAG, "Configuring sensors...");
    for (sensor_types type = 0; type < ESPMOBE32_SENSOR_MAX; type++) {
        if (!sensor_list[type])
            continue;

        sensor_handle *handle = sensor_list[type];

        if (handle->sensor.configure) {
            handle->sensor.configure(handle);
        }
    }
}

void publish_sensor_discoveries(sensor_handle *sensor_list[], esp_mqtt_client_handle_t mqtt)
{
    ESP_LOGI(TAG, "Publish sensor discoveries...");
    for (sensor_types type = 0; type < ESPMOBE32_SENSOR_MAX; type++) {
        if (!sensor_list[type])
            continue;

        sensor_handle *handle = sensor_list[type];

        if (handle->sensor.publish_discovery) {
            handle->sensor.publish_discovery(handle, mqtt);
        }
    }
}

bool publish_sensor_values(sensor_handle *sensor_list[], esp_mqtt_client_handle_t mqtt)
{
    bool done = true;

    ESP_LOGI(TAG, "Publish sensor values...");
    for (sensor_types type = 0; type < ESPMOBE32_SENSOR_MAX; type++) {
        if (!sensor_list[type])
            continue;

        sensor_handle *handle = sensor_list[type];

        if (!handle->sensor.publish_values)
            continue;

        switch (handle->progress) {
        case ESPMOBE32_SP_INITIALIZING:
        case ESPMOBE32_SP_MEASURING:
            ESP_LOGI(TAG, "Sensor still measuring... %i", (uint32_t)type);

            done = false;
            break;

        case ESPMOBE32_SP_MEASURED:
            ESP_LOGI(TAG, "Publish sensor values... %i", (uint32_t)type);

            handle->sensor.publish_values(handle, mqtt);
            break;

        case ESPMOBE32_SP_UNKNOWN:
        case ESPMOBE32_SP_FAILED:
        case ESPMOBE32_SP_PUBLISHED:
        default:
            break;
        }
    }

    return done;
}

void register_mqtt_topic(sensor_handle *sensor_list[], esp_mqtt_client_handle_t mqtt)
{
    ESP_LOGI(TAG, "Register sensor mqtt topics...");
    for (sensor_types type = 0; type < ESPMOBE32_SENSOR_MAX; type++) {
        if (!sensor_list[type])
            continue;

        sensor_handle *handle = sensor_list[type];

        if (handle->sensor.register_mqtt_topic) {
            handle->sensor.register_mqtt_topic(handle, mqtt);
        }
    }

}

void abort_sensors(sensor_handle *sensor_list[])
{
    ESP_LOGI(TAG, "Aborting sensors...");
    for (sensor_types type = 0; type < ESPMOBE32_SENSOR_MAX; type++) {
        if (!sensor_list[type])
            continue;

        sensor_handle *handle = sensor_list[type];

        if (handle->progress > ESPMOBE32_SP_MEASURING)
            continue;

        if (handle->sensor.abort) {
            handle->sensor.abort(handle);
        }
    }
}
