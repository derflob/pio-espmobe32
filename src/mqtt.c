#include "esp_err.h"
#include "esp_log.h"

#include "mqtt.h"

#include "mqtt_client.h"

#include "cJSON.h"

#include "sdkconfig.h"
#include "constants.h"

static const char *TAG = "espmobe32-mqtt";

static bool mqtt_connected = false;

static void mqtt_connection_callback(void *args, esp_event_base_t base, int32_t event_id, void *event);

static void get_unique_id(char *name, char *buffer, size_t length);

static cJSON *retrieve_discovery_base_sensor(char *name, char *device_class, char *state_class, char *uom, char *icon, char *attributes_name, bool force_name);
static cJSON *retrieve_discovery_trigger(char *name, char *type, char *subtype, char *payload);
static cJSON *retrieve_device(void);

static void transmit_json(esp_mqtt_client_handle_t mqtt, char *topic, cJSON *json, int qos, bool retrain);

static void _to_lower(char *s)
{
    for ( ; *s; ++s) *s = tolower(*s);
}

esp_mqtt_client_handle_t start_mqtt(void)
{
    const esp_mqtt_client_config_t config = {
        .uri = CONFIG_ESPMOBE32_MQTT_SERVER,
        .username = CONFIG_ESPMOBE32_MQTT_USERNAME,
        .password = CONFIG_ESPMOBE32_MQTT_PASSWORD,
        .client_id = CONFIG_ESPMOBE32_HOSTNAME,
    };

    esp_mqtt_client_handle_t mqtt = esp_mqtt_client_init(&config);

    esp_mqtt_client_register_event(mqtt, MQTT_EVENT_CONNECTED, &mqtt_connection_callback, NULL);
    esp_mqtt_client_register_event(mqtt, MQTT_EVENT_DISCONNECTED, &mqtt_connection_callback, NULL);

    esp_mqtt_client_start(mqtt);

    return mqtt;
}

static void mqtt_connection_callback(void *args, esp_event_base_t base, int32_t event_id, void *event_data)
{
    esp_mqtt_event_handle_t event = (esp_mqtt_event_handle_t)event_data;

    if (event->event_id == MQTT_EVENT_CONNECTED) {
        ESP_LOGI(TAG, "Connected.");
        mqtt_connected = true;
    } else if (event->event_id == MQTT_EVENT_DISCONNECTED) {
        ESP_LOGI(TAG, "Disconnected.");
        mqtt_connected = false;
    }
}

bool is_mqtt_connected()
{
    return mqtt_connected;
}

static cJSON *retrieve_device(void)
{
    cJSON *device = cJSON_CreateObject();

    cJSON_AddStringToObject(device, "name", CONFIG_ESPMOBE32_HOSTNAME);
    cJSON_AddStringToObject(device, "identifiers", CONFIG_ESPMOBE32_HOSTNAME);
    cJSON_AddStringToObject(device, "manufacturer", "mebtech & Flobs");
    cJSON_AddStringToObject(device, "model", HARDWARE_MODEL);
    cJSON_AddStringToObject(device, "sw_version", VERSION);

    return device;
}

void get_discovery_topic(char *name, char *type, char *buffer, size_t length)
{
    char lower_name[MAX_MQTT_UNIQUE_ID_LENGTH] = {'\0'};
    strncpy(lower_name, name, MAX_MQTT_UNIQUE_ID_LENGTH);
    _to_lower(lower_name);
    snprintf(buffer, length, "homeassistant/%s/%s/%s/config", type, CONFIG_ESPMOBE32_HOSTNAME, lower_name);
}

void get_unique_topic(char *name, char *type, char *buffer, size_t length)
{
    char unique_id[MAX_MQTT_UNIQUE_ID_LENGTH] = {'\0'};

    get_unique_id(name, unique_id, MAX_MQTT_UNIQUE_ID_LENGTH);

    snprintf(buffer, length, "%s/%s/%s/%s", CONFIG_ESPMOBE32_MQTT_STATE_TOPIC_PREFIX, CONFIG_ESPMOBE32_HOSTNAME, unique_id, type);
}

void get_state_topic(char *name, char *buffer, size_t length)
{
    get_unique_topic(name, "state", buffer, length);
}

void get_trigger_topic(char *name, char *buffer, size_t length)
{
    get_unique_topic(name, "trigger", buffer, length);
}

void get_attributes_topic(char *name, char *buffer, size_t length)
{
    char lower_name[MAX_MQTT_UNIQUE_ID_LENGTH] = {'\0'};
    strncpy(lower_name, name, MAX_MQTT_UNIQUE_ID_LENGTH);
    _to_lower(lower_name);
    snprintf(buffer, length, "%s/%s/%s/attributes", CONFIG_ESPMOBE32_MQTT_STATE_TOPIC_PREFIX, CONFIG_ESPMOBE32_HOSTNAME, lower_name);
}

void get_settings_topic(char *name, char *buffer, size_t length)
{
    char lower_name[MAX_MQTT_UNIQUE_ID_LENGTH] = {'\0'};
    strncpy(lower_name, name, MAX_MQTT_UNIQUE_ID_LENGTH);
    _to_lower(lower_name);
    snprintf(buffer, length, "%s/%s/%s/settings", CONFIG_ESPMOBE32_MQTT_STATE_TOPIC_PREFIX, CONFIG_ESPMOBE32_HOSTNAME, lower_name);
}

static void get_unique_id(char *name, char *buffer, size_t length)
{
    char lower_name[MAX_MQTT_UNIQUE_ID_LENGTH] = {'\0'};
    strncpy(lower_name, name, MAX_MQTT_UNIQUE_ID_LENGTH);
    _to_lower(lower_name);
    snprintf(buffer, length, "%s_%s", CONFIG_ESPMOBE32_HOSTNAME, lower_name);
}

static cJSON *retrieve_discovery_base_sensor(char *name, char *device_class, char *state_class, char *uom, char *icon, char *attributes_name, bool force_name)
{
    char state_topic[MAX_MQTT_TOPIC_LENGTH] = {'\0'};
    char attributes_topic[MAX_MQTT_TOPIC_LENGTH] = {'\0'};

    char unique_id[MAX_MQTT_UNIQUE_ID_LENGTH] = {'\0'};

    get_state_topic(name, state_topic, MAX_MQTT_TOPIC_LENGTH);
    get_unique_id(name, unique_id, MAX_MQTT_UNIQUE_ID_LENGTH);

    cJSON *discovery = cJSON_CreateObject();

    cJSON_AddStringToObject(discovery, "unique_id", unique_id);
    if (force_name) {
        cJSON_AddStringToObject(discovery, "name", name);
    }
    cJSON_AddStringToObject(discovery, "state_topic", state_topic);
    cJSON_AddNumberToObject(discovery, "expire_after", (uint32_t)(CONFIG_ESPMOBE32_PERIODIC_WAKEUP_MS * 1.25 / 1000));

    if (device_class)
        cJSON_AddStringToObject(discovery, "device_class", device_class);

    if (state_class)
        cJSON_AddStringToObject(discovery, "state_class", state_class);

    if (uom)
        cJSON_AddStringToObject(discovery, "unit_of_measurement", uom);

    if (icon)
        cJSON_AddStringToObject(discovery, "icon", icon);

    if (attributes_name) {
        get_attributes_topic(attributes_name, attributes_topic, MAX_MQTT_TOPIC_LENGTH);
        cJSON_AddStringToObject(discovery, "json_attributes_topic", attributes_topic);
    }

    cJSON_AddItemToObject(discovery, "device", retrieve_device());

    return discovery;
}

static cJSON *retrieve_discovery_trigger(char *name, char *type, char *subtype, char *payload)
{
    char trigger_topic[MAX_MQTT_TOPIC_LENGTH] = {'\0'};
    get_trigger_topic(name, trigger_topic, MAX_MQTT_TOPIC_LENGTH);

    cJSON *discovery = cJSON_CreateObject();
    cJSON_AddStringToObject(discovery, "automation_type", "trigger");
    cJSON_AddStringToObject(discovery, "topic", trigger_topic);
    cJSON_AddStringToObject(discovery, "type", type);
    cJSON_AddStringToObject(discovery, "subtype", subtype);

    if (payload)
        cJSON_AddStringToObject(discovery, "payload", payload);

    cJSON_AddItemToObject(discovery, "device", retrieve_device());

    return discovery;
}

void publish_discovery_sensor(esp_mqtt_client_handle_t mqtt, char *name, char *device_class, char *state_class, char *uom, char *icon, char *attributes_name, bool force_name)
{
    char discovery_topic[MAX_MQTT_TOPIC_LENGTH] = {'\0'};
    get_discovery_topic(name, "sensor", discovery_topic, MAX_MQTT_TOPIC_LENGTH);

    cJSON *discovery = retrieve_discovery_base_sensor(name, device_class, state_class, uom, icon, attributes_name, force_name);

    transmit_json(mqtt, discovery_topic, discovery, 0, true);

    cJSON_Delete(discovery);
}

void publish_discovery_binary_sensor(esp_mqtt_client_handle_t mqtt, char *name, char *device_class, char *attributes_name, char *payload_on, char *payload_off, bool force_name)
{
    char discovery_topic[MAX_MQTT_TOPIC_LENGTH] = {'\0'};
    get_discovery_topic(name, "binary_sensor", discovery_topic, MAX_MQTT_TOPIC_LENGTH);

    cJSON *discovery = retrieve_discovery_base_sensor(name, device_class, NULL, NULL, NULL, attributes_name, force_name);

    if (payload_on)
        cJSON_AddStringToObject(discovery, "payload_on", payload_on);

    if (payload_off)
        cJSON_AddStringToObject(discovery, "payload_off", payload_off);

    transmit_json(mqtt, discovery_topic, discovery, 0, true);

    cJSON_Delete(discovery);
}

void publish_discovery_trigger(esp_mqtt_client_handle_t mqtt, char *name, char *discovery_name, char *type, char *subtype, char *payload)
{
    char discovery_topic[MAX_MQTT_TOPIC_LENGTH] = {'\0'};
    get_discovery_topic(discovery_name, "device_automation", discovery_topic, MAX_MQTT_TOPIC_LENGTH);

    cJSON *discovery = retrieve_discovery_trigger(name, type, subtype, payload);

    transmit_json(mqtt, discovery_topic, discovery, 0, true);

    cJSON_Delete(discovery);
}

static void transmit_json(esp_mqtt_client_handle_t mqtt, char *topic, cJSON *json, int qos, bool retrain)
{
    char *data = cJSON_PrintUnformatted(json);

    if (data) {
        esp_mqtt_client_publish(mqtt, topic, data, strlen(data), qos, retrain);

        free(data);
    }
}

void publish_data_string(esp_mqtt_client_handle_t mqtt, char *name, char *value, bool retain)
{
    char topic[MAX_MQTT_TOPIC_LENGTH] = {'\0'};
    get_state_topic(name, topic, MAX_MQTT_TOPIC_LENGTH);

    esp_mqtt_client_publish(mqtt, topic, value, strlen(value),  1, retain);
}

void publish_data_float_with_decimals(esp_mqtt_client_handle_t mqtt, char *name, float value, bool retain, uint8_t decimals)
{
    char str_value[32] = {'\0'};
    snprintf(str_value, 32, "%1.*f", decimals, value);

    publish_data_string(mqtt, name, str_value, retain);
}

void publish_data_float(esp_mqtt_client_handle_t mqtt, char *name, float value, bool retain) { publish_data_float_with_decimals(mqtt, name, value, retain, 2); };

void publish_data_uint32(esp_mqtt_client_handle_t mqtt, char *name, uint32_t value, bool retain)
{
    char str_value[32] = {'\0'};
    snprintf(str_value, 32, "%u", value);

    publish_data_string(mqtt, name, str_value, retain);
}

void publish_data_int32(esp_mqtt_client_handle_t mqtt, char *name, int32_t value, bool retain)
{
    char str_value[32] = {'\0'};
    snprintf(str_value, 32, "%i", value);

    publish_data_string(mqtt, name, str_value, retain);
}

void publish_data_attributes(esp_mqtt_client_handle_t mqtt, char *name, cJSON *json, bool retain)
{
    char topic[MAX_MQTT_TOPIC_LENGTH] = {'\0'};
    get_attributes_topic(name, topic, MAX_MQTT_TOPIC_LENGTH);

    transmit_json(mqtt, topic, json, 1, retain);
}

void publish_trigger(esp_mqtt_client_handle_t mqtt, char *name, char *value, bool retain)
{
    char topic[MAX_MQTT_TOPIC_LENGTH] = {'\0'};
    get_trigger_topic(name, topic, MAX_MQTT_TOPIC_LENGTH);

    esp_mqtt_client_publish(mqtt, topic, value, strlen(value), 1, retain);
}
