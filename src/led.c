#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_timer.h"
#include "driver/ledc.h"
#include "driver/rtc_io.h"
#include "esp_log.h"
#include "esp_err.h"

#include "led.h"
#include "constants.h"
#include "pins.h"

#include "wifi.h"
#include "ota.h"

static const char *TAG = "status-led";

static bool _stop_status_led = false;

void breath_led_status(uint32_t speed, uint32_t min_duty, uint32_t max_duty)
{
    enum breath_sections {
        BREATH_IN = 0,
        BREATH_OUT = 1,
        BREATH_HOLD = 2
    };

    static enum breath_sections current_breath_section = BREATH_HOLD;

    uint64_t time = esp_timer_get_time() / 1000;
    enum breath_sections new_breath_section = ((time % speed) * 3) / speed;

    if (current_breath_section != new_breath_section) {
        ESP_LOGD(TAG, "Now breathing %i", new_breath_section);
        switch (new_breath_section) {
            case BREATH_IN:
                ESP_ERROR_CHECK(ledc_set_fade_time_and_start(
                    LEDC_HIGH_SPEED_MODE,
                    LED_STATUS_LEDC_CHANNEL,
                    max_duty,
                    speed / 3,
                    LEDC_FADE_NO_WAIT
                ));
                break;
            case BREATH_OUT:
                ESP_ERROR_CHECK(ledc_set_fade_time_and_start(
                    LEDC_HIGH_SPEED_MODE,
                    LED_STATUS_LEDC_CHANNEL,
                    min_duty,
                    speed / 3,
                    LEDC_FADE_NO_WAIT
                ));
                break;
            default:
                break;
        }
        current_breath_section = new_breath_section;

    } else if (current_breath_section == BREATH_HOLD) {
        if (ledc_get_duty(LEDC_HIGH_SPEED_MODE, LED_STATUS_LEDC_CHANNEL) != min_duty) {
            uint64_t time_left_in_hold = speed - (time % speed);
            ESP_ERROR_CHECK(ledc_set_fade_time_and_start(
                LEDC_HIGH_SPEED_MODE,
                LED_STATUS_LEDC_CHANNEL,
                min_duty,
                time_left_in_hold / 2,
                LEDC_FADE_NO_WAIT
            ));
        }
    }
}

static void status_led_task(void *pvParameters)
{
    ESP_LOGI(TAG, "Starting status led.");

    const ledc_timer_config_t timer = {
        .speed_mode = LEDC_HIGH_SPEED_MODE,
        .duty_resolution = LED_STATUS_DUTY_CYCLE_BITS,
        .timer_num = LEDC_TIMER_1,
        .freq_hz = LED_STATUS_FREQUENCY,
        .clk_cfg = LEDC_AUTO_CLK
    };

    const ledc_channel_config_t channel = {
        .gpio_num = LED_STATUS,
        .channel = LED_STATUS_LEDC_CHANNEL,
        .speed_mode = timer.speed_mode,
        .intr_type = LEDC_INTR_DISABLE,
        .timer_sel = timer.timer_num,
        .duty = 0,
        .hpoint = 0
    };

    ESP_ERROR_CHECK(ledc_channel_config(&channel));
    ESP_ERROR_CHECK(ledc_timer_config(&timer));

    ledc_fade_func_install(0);

    for (;;) {
        if (_stop_status_led)
            break;
        
        if (is_ota_performed() || is_ota_in_progress())
            breath_led_status(LED_STATUS_BREATH_SPEED / 4, 4, LED_STATUS_DUTY_CYCLE_MAX / 2);
        else if (is_wifi_ap_open())
            breath_led_status(LED_STATUS_BREATH_SPEED, 4, LED_STATUS_DUTY_CYCLE_MAX / 2);
        else if (esp_log_timestamp() > CONFIG_ESPMOBE32_SLOW_RUN_INDICATOR_TIME)
            breath_led_status(LED_STATUS_BREATH_SPEED / 2, 4, LED_STATUS_DUTY_CYCLE_MAX / 2);
        else
            ledc_set_duty_and_update(LEDC_HIGH_SPEED_MODE, LED_STATUS_LEDC_CHANNEL, 0, 0);

        vTaskDelay(pdMS_TO_TICKS(50));
    }

    vTaskDelete(NULL);
}

void start_status_led(void) {
    gpio_set_level(LED_STATUS, 0);
    gpio_set_direction(LED_STATUS, GPIO_MODE_OUTPUT);
    gpio_set_pull_mode(LED_STATUS, GPIO_PULLUP_PULLDOWN);

    rtc_gpio_deinit(LED_STATUS);

    xTaskCreatePinnedToCore(&status_led_task, TAG, 2048, NULL, 15, NULL, PRO_CPU_NUM);
}

void stop_status_led(void) {
    _stop_status_led = true;

    ledc_fade_func_uninstall();
    ledc_stop(LEDC_HIGH_SPEED_MODE, LED_STATUS_LEDC_CHANNEL, 0);

    /* Configure for deep sleep*/
    rtc_gpio_init(LED_STATUS);
    rtc_gpio_set_direction(LED_STATUS, RTC_GPIO_MODE_OUTPUT_ONLY);
    rtc_gpio_pullup_en(LED_STATUS);
    rtc_gpio_set_level(LED_STATUS, 0);
    rtc_gpio_hold_en(LED_STATUS);
}
