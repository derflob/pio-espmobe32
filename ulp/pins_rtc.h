#ifndef __ESPM32_PINS_RTC_H__
#define __ESPM32_PINS_RTC_H__

#include "soc/rtc_io_reg.h"

#define LED_STATUS_RTC_REG RTC_IO_TOUCH_PAD7_REG
#define LED_STATUS_RTC_HOLD RTC_IO_TOUCH_PAD7_HOLD_S
#define LED_STATUS_RTC_NUM 17

#define PIR_INPUT_RTC_NUM 4

#endif
